//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_WRITEFILE_H
#define LAB2_WRITEFILE_H
#include "Worker.h"
#include <fstream>
#include <vector>

class Writefile: public Worker {
public:
    explicit Writefile(std::vector<std::string> parameters);
    explicit Writefile(std::string file_name);
    Writefile();
    ~Writefile() override;

    text execute(text previous_result) override;
    int get_count_parameters() const override{return count_parameters;};
private:
    int count_parameters = 1;
    std::string name;
};


#endif //LAB2_WRITEFILE_H
