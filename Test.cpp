//
// Created by Vatson on 009 09.12.2020.
//

#include "Test.h"
#include "WorkflowExecutor.h"
#include "IParser.h"
#include "IValidator.h"
#include "Readfile.h"
#include "Writefile.h"
#include "Sort.h"
#include "Dump.h"
#include "Grep.h"
#include "Replace.h"
#include <gtest/gtest.h>

int Test::test_start(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(Grep, execute) {
    text data = {"Potato Mango",
                 "Tomato Apple Garlic",
                 "Carrot Parsley Garlic",
                 "Cabbage Lemon",
                 "Pepper Cucumber Kiwi Carrot",
                 "Chili Kiwi Lime",
                 "Eggplant Mango Apple Parsley",
                 "Kiwi Garlic",
                 "Onion Banana Garlic",
                 "Garlic Lime",
                 "Parsley Banana Lime"};
    std::vector<std::string> parameters = {"Garlic"};

    data = Grep(parameters).execute(data);
    ASSERT_TRUE(data.size() == 5);

    text data1 = {"Potato Mango",
                 "Tomato Apple Garlic",
                 "Carrot Parsley Garlic",
                 "Cabbage Lemon",
                 "Pepper Cucumber Kiwi Carrot",
                 "Chili Kiwi Lime",
                 "Eggplant Mango Apple Parsley",
                 "Kiwi Garlic",
                 "Onion Banana Garlic",
                 "Garlic Lime",
                 "Parsley Banana Lime"};
    std::vector<std::string> parameters1 = {"le Pa"};
    data1 = Grep(parameters1).execute(data1);
    ASSERT_TRUE(data1[0] == "Eggplant Mango Apple Parsley");
}



TEST(Sort, execute){
    text data = {"Potato Mango",
                 "Tomato Apple Garlic",
                 "Carrot Parsley Garlic",
                 "Cabbage Lemon",
                 "Pepper Cucumber Kiwi Carrot",
                 "Chili Kiwi Lime",
                 "Banana",
                 "Eggplant Mango Apple Parsley",
                 "Kiwi Garlic",
                 "Apple Cucumber",
                 "Onion Banana Garlic",
                 "Garlic Lime",
                 "Parsley Banana Lime"};
    std::vector<std::string> parameters = {};
    data = Sort(parameters).execute(data);
    ASSERT_TRUE(data[0] == "Apple Cucumber");
    ASSERT_TRUE(data[1] == "Banana");
    ASSERT_TRUE(data[12] == "Tomato Apple Garlic");


}

TEST(Replace, execute){
    text data = {"Potato Mango",
                 "Tomato Apple Garlic",
                 "Carrot Parsley Garlic",
                 "Cabbage Lemon",
                 "Pepper Cucumber Kiwi Carrot",
                 "Chili Kiwi Lime",
                 "Banana",
                 "Eggplant Mango Apple Parsley",
                 "Kiwi Garlic",
                 "Apple Cucumber",
                 "Onion Banana Garlic",
                 "Garlic Lime",
                 "Parsley Banana Apple"};

    std::vector<std::string> parameters = {"Apple", "Carrots"};
    data = Replace(parameters).execute(data);
    ASSERT_TRUE(data[1] == "Tomato Carrots Garlic");
    ASSERT_TRUE(data[7] == "Eggplant Mango Carrots Parsley");
    ASSERT_TRUE(data[9] == "Carrots Cucumber");
}

TEST(Validator, validate){

    std::ifstream file;
    file.open("in_val_test_count_of_parameters.txt");
    schema* our_schema;
    our_schema = Parser().parse(&file);
    ASSERT_THROW(Validator().validate(our_schema), ValidatorException);

}