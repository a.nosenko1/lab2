//
// Created by Vatson on 007 07.12.2020.
//

#include "IParser.h"

schema* Parser::parse(std::ifstream *file) {
    std::map<uint64_t,WorkerArgument> block_id_to_argument;
    std::list<uint64_t> list_of_blocks_id;
    if (!file->is_open()){
        throw std::runtime_error("parser got a closed file, please open previously");
    }
    std::string line; // буфер для строки
    std::string word; // буфер для слова
    uint64_t id; // буфер для id
    WorkerArgument wa; // буфер для параметров

    getline(*file, line); // скип первой строки
    while(getline(*file, line)){ // пока не достигнут конец файла считываем строку
        if (line == "csed")
            break;
        std::istringstream iss(line, std::istringstream::in); // превращаем в поток
        iss >> word; // считываем первое слово, это id
        id = std::stoi(word);
        if (block_id_to_argument.find(id) != block_id_to_argument.end()){
            throw std::runtime_error("there are duplicate id");
        }
        iss >> word; // пропуск =
        iss >> word; // имя блока
        wa.name = word;
        while(iss >> word){ // остальные слова вектор, это аргументы блока
            wa.parameters.push_back(word);
        }
        block_id_to_argument.insert(std::make_pair(id,wa));
        wa.parameters.clear();
    }
    getline(*file, line); // строка со структурой
    std::istringstream iss(line, std::istringstream::in); // превращаем в поток
    int k = 0;
    while(iss >> word){ // id, стрелочка, id
        if(!(k % 2)){
            id = std::stoi(word);
            list_of_blocks_id.push_back(id);
        }
        k++;
    }
 //   copy(list_of_blocks_id.begin(), list_of_blocks_id.end(), std::ostream_iterator<int>(std::cout," "));
    return new schema{block_id_to_argument, list_of_blocks_id};
}
