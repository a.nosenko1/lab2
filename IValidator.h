#ifndef LAB2_IVALIDATOR_H
#define LAB2_IVALIDATOR_H
#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include <string>
#include <vector>
struct schema;

class IValidator {
public:
    virtual void validate(schema* test_schema) = 0;
};

class Validator: public IValidator {
public:
    void validate(schema* test_schema) override;
};

class ValidatorException: public std::exception{
public:
    ValidatorException(std::string information, std::string id){
        this->information = std::move(information);
        this->id = std::move(id);
    }
    std::string what(){return information;}
    std::string where(){return id;}
private:
    std::string information;
    std::string id;
};

#endif //LAB2_IVALIDATOR_H
