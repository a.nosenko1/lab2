#include "Workflow.h"
#include "Test.h"

int main(int argc, char** argv) {
    Test().test_start(argc, argv);
    Workflow().start("schema.txt"); // argv[1]
    return 0;
}

