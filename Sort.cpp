//
// Created by Vatson on 007 07.12.2020.
//

#include "Sort.h"

text Sort::execute(text previous_result) {
    sort(previous_result.begin(), previous_result.end(), std::greater<>());
    std::reverse(previous_result.begin(), previous_result.end());
    return previous_result;
}

Sort::Sort(const std::vector<std::string>& parameters) {

}

Sort::Sort() = default;

