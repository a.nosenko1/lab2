//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_REPLACE_H
#define LAB2_REPLACE_H
#include "Worker.h"
#include <sstream>
class Replace: public Worker {
public:
    explicit Replace(std::vector<std::string> parameters);
    Replace();
    ~Replace() override;

    text execute(text previous_result) override;
    int get_count_parameters() const override{return count_parameters;};
private:
    int count_parameters = 2;
    std::string word1;
    std::string word2;
};


#endif //LAB2_REPLACE_H
