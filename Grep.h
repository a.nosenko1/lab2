//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_GREP_H
#define LAB2_GREP_H
#include "Worker.h"


class Grep: public Worker {
public:
    explicit Grep(std::vector<std::string> parameters);
    Grep();
    ~Grep() override;

    text execute(text previous_result) override;
    int get_count_parameters() const override{return count_parameters;};
private:
    int count_parameters = 1;
    std::string word;
};


#endif //LAB2_GREP_H
