//
// Created by Vatson on 013 13.12.2020.
//

#ifndef LAB2_WORKFLOW_H
#define LAB2_WORKFLOW_H
#include "IParser.h"
#include "WorkflowExecutor.h"

class Workflow {
public:
    void start(const std::string& file_name);
private:
    std::ifstream file;
};


#endif //LAB2_WORKFLOW_H
