//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_READFILE_H
#define LAB2_READFILE_H
#include "Worker.h"
#include <fstream>


class Readfile : public Worker {
public:
    explicit Readfile(std::vector<std::string> parameters);
    Readfile();
    ~Readfile() override;

    text execute(text previous_result) override;
    int get_count_parameters() const override { return count_parameters; };
private:
    int count_parameters = 1;
    std::ifstream file;
};


#endif //LAB2_READFILE_H
