cmake_minimum_required(VERSION 3.16)
project(lab2)

set(CMAKE_CXX_STANDARD 14)
add_subdirectory(lib/googletests)
include_directories(lib/googletests/googletest/include)

add_executable(lab2 main.cpp Worker.h Readfile.cpp Readfile.h Writefile.cpp Writefile.h Grep.cpp Grep.h Sort.cpp Sort.h Replace.cpp Replace.h Dump.cpp Dump.h IParser.cpp IParser.h IValidator.cpp IValidator.h WorkflowExecutor.cpp WorkflowExecutor.h Test.cpp Test.h WorkerFactory.cpp WorkerFactory.h Workflow.cpp Workflow.h)
target_link_libraries(lab2 gtest)