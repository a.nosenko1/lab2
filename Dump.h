//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_DUMP_H
#define LAB2_DUMP_H
#include "Worker.h"

class Dump: public Worker {
public:
    explicit Dump(std::vector<std::string> parameters);
    Dump();

    text execute(text previous_result) override;
    int get_count_parameters() const override{return count_parameters;};
private:
    int count_parameters = 1;
    std::string name;
};


#endif //LAB2_DUMP_H
