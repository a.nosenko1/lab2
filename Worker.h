//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_WORKER_H
#define LAB2_WORKER_H
#include <string>
#include <iostream>
#include <vector>

#define text std::vector<std::string>

class Worker {
public:
    virtual text execute(text previous_result) = 0;
    virtual ~Worker()= default;
    virtual int get_count_parameters() const{return count_parameters;};
private:
    int count_parameters;
};

#endif //LAB2_WORKER_H
