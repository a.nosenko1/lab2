#include "IValidator.h"
#include "IParser.h"
#include "Replace.h"
#include "WorkerFactory.h"
#include "Readfile.h"
#include "Writefile.h"
#include "Sort.h"
#include "Dump.h"
#include "Grep.h"


void Validator::validate(schema *test_schema) {
    Sort w1;
    Dump w2;
    Writefile w3;
    Readfile w4;
    Grep w5;
    Replace w6;
    std::map<std::string,Worker*> a;
    a.insert(std::make_pair("sort", &w1));
    a.insert(std::make_pair("dump", &w2));
    a.insert(std::make_pair("writefile", &w3));
    a.insert(std::make_pair("readfile", &w4));
    a.insert(std::make_pair("grep", &w5));
    a.insert(std::make_pair("replace", &w6));

    for (const auto& id:test_schema->block_id_to_argument) {
        if (a.find(id.second.name) != a.end())
            if (a.find(id.second.name)->second->get_count_parameters() != id.second.parameters.size())
                throw ValidatorException("worst count of parameters in block id ", std::to_string(id.first));
    }

    if (test_schema->block_id_to_argument.find(test_schema->list_of_blocks_id.front())->second.name != "readfile"){
        throw ValidatorException("first block musts be readfile ", "");
    }
    if (test_schema->block_id_to_argument.find(test_schema->list_of_blocks_id.back())->second.name != "writefile"){
        throw ValidatorException("last block musts be writefile ", "");
    }

    auto it1 = test_schema->list_of_blocks_id.begin();
    it1++; // начинаем со второго
    for (int j = 0; j < test_schema->list_of_blocks_id.size()-2; j++, it1++) { // -2 т.к. первый и последний не проверяем
        if (test_schema->block_id_to_argument.find(*it1)->second.name == "readfile"){
            throw ValidatorException("the middle block mustn't be readfile, position in schema: ",
                                     std::to_string(j +1));
        }
        if (test_schema->block_id_to_argument.find(*it1)->second.name == "writefile"){
            throw ValidatorException("the middle block mustn't be writefile, position in schema: ",
                                     std::to_string(j +1));
        }
    }
}

