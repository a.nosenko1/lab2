//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_IPARSER_H
#define LAB2_IPARSER_H

#include <map>
#include <list>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>
#include <iterator>

struct WorkerArgument{
    std::string name;
    std::vector<std::string> parameters;
};

struct schema{
    std::map<uint64_t,WorkerArgument> block_id_to_argument;
    std::list<uint64_t> list_of_blocks_id;
};

class IParser {
public:
    virtual schema* parse(std::ifstream* file) = 0;
};

class Parser: public IParser{
public:
    schema* parse(std::ifstream* file) override;
};

#endif //LAB2_IPARSER_H
