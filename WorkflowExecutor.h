//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_WORKFLOWEXECUTOR_H
#define LAB2_WORKFLOWEXECUTOR_H
#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include <utility>
#include <vector>
#include <exception>

class Writefile;
class Readfile;
class Grep;
class Dump;
class Replace;
class Sort;
class IValidator;
class IParser;
class Parser;
class Worker;
struct schema;

class WorkflowExecutor {
public:
    explicit WorkflowExecutor();
    ~WorkflowExecutor();
    static void execute(schema* our_schema);
};

#endif //LAB2_WORKFLOWEXECUTOR_H
