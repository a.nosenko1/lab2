//
// Created by Vatson on 007 07.12.2020.
//

#ifndef LAB2_SORT_H
#define LAB2_SORT_H
#include "Worker.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class Sort: public Worker {
public:
    explicit Sort(const std::vector<std::string>& parameters);
    text execute(text previous_result) override;
    Sort();
    ~Sort() override= default;
    int get_count_parameters() const override{return count_parameters;};
private:
    int count_parameters = 0;
};

#endif //LAB2_SORT_H
