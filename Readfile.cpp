//
// Created by Vatson on 007 07.12.2020.
//

#include "Readfile.h"

text Readfile::execute(text previous_result) {
    std::string str;
    text vec;
    while (getline(file, str)){
        vec.push_back(str);
    }
    return vec;
}

Readfile::Readfile(std::vector<std::string> parameters) {
    file.open(parameters[0]);
    if (!file.is_open()) {
        throw std::runtime_error("Cannot open file (in Readfile)");
    }
}

Readfile::~Readfile(){
    if (file.is_open()) {
        file.close();
    }
}



Readfile::Readfile() = default;
