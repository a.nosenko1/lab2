//
// Created by Vatson on 007 07.12.2020.
//

#include "Dump.h"
#include "Writefile.h"



text Dump::execute(std::vector<std::string> previous_result) {
    Writefile(this->name).execute(previous_result);
    return previous_result;
}

Dump::Dump(std::vector<std::string> parameters) {
    this->name = std::move(parameters[0]);
}

Dump::Dump() = default;


