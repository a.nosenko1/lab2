//
// Created by Vatson on 007 07.12.2020.
//

#include "Grep.h"

#include <algorithm>
#include <sstream>
#include <cstring>
#include <string>

text Grep::execute(text previous_result) { // посик по лексеме
    for (auto it = std::begin(previous_result); it != std::end(previous_result); ++it) {
        if (strstr(it->c_str(), word.c_str()) == nullptr) {
            previous_result.erase(it);
            it--;
        }
    }
    return previous_result;
}

Grep::Grep(std::vector<std::string> parameters) {
    this->word = std::move(parameters[0]);
}

Grep::~Grep() = default;
Grep::Grep() = default;
