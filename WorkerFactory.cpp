#include "WorkerFactory.h"
#include "IParser.h"
#include "IValidator.h"
#include "Readfile.h"
#include "Writefile.h"
#include "Sort.h"
#include "Dump.h"
#include "Grep.h"
#include "Replace.h"

Worker *WorkerFactory::createWorker(WorkerArgument *arguments) {
    if (arguments->name == "readfile"){
        auto* r = new Readfile(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "readfile");
        }
        return r;
    }
    if (arguments->name == "writefile"){
        auto* r = new Writefile(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "writefile");
        }
        return r;
    }
    if (arguments->name == "replace"){
        auto* r = new Replace(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "replace");
        }
        return r;
    }
    if (arguments->name == "sort"){
        auto* r = new Sort(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "sort");
        }
        return r;
    }
    if (arguments->name == "dump"){
        auto* r = new Dump(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "dump");
        }
        return r;
    }
    if (arguments->name == "grep"){
        auto* r = new Grep(arguments->parameters);
        if(r->get_count_parameters() != arguments->parameters.size()){
            throw ValidatorException("wrong count of parameters in block ", "dump");
        }
        return r;
    }
    throw std::runtime_error("requested non-existent Worker in WorkerFactory/createWorker");
}
