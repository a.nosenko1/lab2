#include "Workflow.h"
#include "IValidator.h"

void Workflow::start(const std::string& file_name) {
    try {
        file.open(file_name);
        if (!file.is_open()) {
            throw std::runtime_error("Cannot open file (in Workflow)");
        }
        schema* our_schema = Parser().parse(&file);
        WorkflowExecutor().execute(our_schema);
    }
    catch (ValidatorException &ex) {
        std::cout << ex.what() << ex.where() << std::endl;
    }
    catch (std::exception &ex) {
        std::cout << ex.what() << std::endl;
    }
    if (file.is_open()) {
        file.close();
    }
}
