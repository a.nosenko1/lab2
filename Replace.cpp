//
// Created by Vatson on 007 07.12.2020.
//

#include "Replace.h"

Replace::Replace(std::vector<std::string> parameters) {
    this->word1 = parameters[0];
    this->word2 = parameters[1];
}

text Replace::execute(std::vector<std::string> previous_result) {
    if (word1 != word2){
        for (auto & str : previous_result) {
            std::stringstream data(str);
            std::string tmp_word;
            std::string new_str;
            while(std::getline(data, tmp_word, ' '))
                new_str.append((tmp_word == word1 ? word2 : tmp_word) + " ");
            new_str.pop_back(); // лишний пробел
            str = new_str;
        }
    }
    return previous_result;
}

Replace::~Replace() = default;
Replace::Replace() = default;


