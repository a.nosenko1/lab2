//
// Created by Vatson on 007 07.12.2020.
//

#include "Writefile.h"

#include <utility>

text Writefile::execute(text previous_result) {
    std::fstream file;
    file.open(name, std::ios::out);
    if (!file.is_open())
        throw std::runtime_error("Cannot open file (in Writefile)");
    for (auto & str : previous_result) {
        file << str << std::endl;
    }
    if (file.is_open()) {
        file.close();
    }
    return std::vector<std::string>();
}

Writefile::Writefile(std::vector<std::string> parameters) {
    this->name = parameters[0];
}

Writefile::Writefile(std::string file_name) {
    this->name = std::move(file_name);
}

Writefile::~Writefile() = default;
Writefile::Writefile() = default;
