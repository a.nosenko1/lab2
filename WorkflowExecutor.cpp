#include "WorkflowExecutor.h"
#include "WorkerFactory.h"
#include "IParser.h"
#include "IValidator.h"
#include "Readfile.h"


WorkflowExecutor::WorkflowExecutor() = default;

WorkflowExecutor::~WorkflowExecutor() = default;

void WorkflowExecutor::execute(schema *our_schema) {
    Validator().validate(our_schema); // OK
    WorkerFactory factory;
    text ret;
    for (const auto &next_block_id : our_schema->list_of_blocks_id) {
        ret = factory.createWorker(&our_schema->block_id_to_argument.find(next_block_id)->second)->execute(ret);
    }
}
