//
// Created by Vatson on 013 13.12.2020.
//

#ifndef LAB2_WORKERFACTORY_H
#define LAB2_WORKERFACTORY_H
#include <string>
#include <vector>

class Worker;
struct WorkerArgument;

class WorkerFactory {
public:
    Worker* createWorker(WorkerArgument* arguments);
};

#endif //LAB2_WORKERFACTORY_H
